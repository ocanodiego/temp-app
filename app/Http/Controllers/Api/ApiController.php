<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    protected $client;

    function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function temp($lat, $lng){

    	$res = $this->client->get('https://api.darksky.net/forecast/52ac0fa8998f79725d16aeb6d98fc4d5/'.$lat.','.$lng);
    
    	return $res;
    	
    }

    public function tempByTime($lat, $lng, $time){

    	$res = $this->client->get('https://api.darksky.net/forecast/52ac0fa8998f79725d16aeb6d98fc4d5/'.$lat.','.$lng.','.$time);
    
    	return $res;
    	
    }
}
